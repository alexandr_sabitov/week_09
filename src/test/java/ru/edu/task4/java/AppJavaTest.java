package ru.edu.task4.java;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppJavaTest {

    @Test
    public void run() {
        assertEquals("cacheService of RealService", AppJava.run().getSomeInterface().getName());
    }
}