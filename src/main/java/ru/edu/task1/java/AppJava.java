package ru.edu.task1.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;


/**
 * Класс для настройки контекста контейнера зависимостей.
 */


@ComponentScan("ru.edu.task1")
public class AppJava {

    public static MainContainer run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }
}
