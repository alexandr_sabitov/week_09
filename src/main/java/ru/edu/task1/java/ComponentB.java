package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
@PropertySource("classpath:task_01.properties")
public class ComponentB {
    private String string;
    @Autowired
    public ComponentB(@Value("${componentB.default.text}")String string) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
