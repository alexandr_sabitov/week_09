package ru.edu.task1.java;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class ComponentC {

    private boolean isInit;

    @PostConstruct
    public void init() {
        isInit = true;
    }


    public boolean isValid() {
        return isInit;
    }
}
