package ru.edu.task5.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class PostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof InterfaceToRemove) {
            return "remove";
        }

        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof InterfaceToWrap) {
            return (InterfaceToWrap) () -> "wrapped " + ((InterfaceToWrap) bean).getValue();
        }
        return bean;
    }
}
