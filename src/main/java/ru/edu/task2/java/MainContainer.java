package ru.edu.task2.java;


import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private TimeKeeper keeper;

    private Child child;

    public MainContainer(TimeKeeper keeperBean, Child childBean) {
        keeper = keeperBean;
        child = childBean;
    }

    public boolean isValid() {
        if(keeper == null || child == null){
            throw new RuntimeException("Есть пустая зависимость");
        }
        if(keeper.equals(child.getTimeKeeper())){
            throw new RuntimeException("Зависимости не должны совпадать");
        }
        return true;
    }
}
