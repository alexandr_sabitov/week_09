package ru.edu.task4.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private SomeInterface someInterface;

    public MainContainer(SomeInterface someInterfaceBean) {
        someInterface = someInterfaceBean;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
