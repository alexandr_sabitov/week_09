package ru.edu.task4.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component("ru.edu.task4.java.RealService")
public class RealService implements SomeInterface {
    @Override
    public String getName() {
        return "RealService";
    }
}
