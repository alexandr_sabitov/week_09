package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private SomeInterface someInterface;
    @Autowired
    public MainContainer(@Qualifier("ru.edu.task4.java.CacheService") SomeInterface someInterfaceBean) {
        someInterface = someInterfaceBean;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
